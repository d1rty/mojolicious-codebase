#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

use Mojolicious::Lite;
use TryCatch;
use MojoX::Log::Log4perl;

use MojoBase::Plugin::Assertions;
use MojoBase::Plugin::RemoteHost;
use MojoBase::Plugin::Authentication;
use MojoBase::Plugin::CORS;
use MojoBase::Plugin::ExecUtil;
use MojoBase::Plugin::UUIDUtil;
use MojoBase::Plugin::ApiKeyGenerator;
use MojoBase::Plugin::Serializer;
use MojoBase::Plugin::Autoresponder;

# configure application
plugin Config => {
        file => '/usr/share/mojobase/config/mojobase.conf'
    };

# configure secrets
app->secrets( (ref app->config->{secrets} eq "ARRAY") ? app->config->{secrets} : [ app->config->{secrets} ] );

# load logger
app->log( MojoX::Log::Log4perl->new( app->config->{'MojoX::Log::Log4perl'}->{file}, 1 ) );

# dynamically load plugins
foreach my $plugin (@{ app->config->{plugins} }) {
    app->log->trace(sprintf('Loading plugin %s', $plugin));
    plugin $plugin => app->config->{$plugin} || {};
}

# execute hook before routing
hook before_routes => sub {
        my ($self) = @_;

        # Authentication plugin
        if (grep { 'MojoBase::Plugin::Authentication' eq $_ } @{ app->config->{plugins} }) {
            try {
                $self->authenticate();
            } catch (MojoBase::Exception::UnauthorizedAccessException $e) {
                return (grep { 'MojoBase::Plugin::Autoresponder' eq $_ } @{ app->config->{plugins} })
                    ? $self->respond( $e )
                    : $self->respond_to ( $e );
            }
        }


        # CORS plugin
        if (grep { 'MojoBase::Plugin::CORS' eq $_ } @{ app->config->{plugins} }) {
            $self->cors();
        }

    };

# answer to preflight requests (CORS)
options '*' => sub {
        my ($self) = @_;

        $self->res->headers->header( 'Access-Control-Allow-Credentials' => 'true' );
        $self->res->headers->header( 'Access-Control-Allow-Methods' => 'OPTIONS, GET, POST, DELETE, PUT' );
        $self->res->headers->header( 'Access-Control-Allow-Headers' => 'Accept', 'Content-Type', 'X-CSRF-Token' );
        $self->res->headers->header( 'Access-Control-Max-Age' => '1728000' );

        $self->respond_to( any => { data => '', status => 200 } );
    };

get '/ping' => sub {
        my ($c) = @_;
        try {
            $c->respond({message=>'pong'});
        } catch (MojoBase::Exception::BasicException $e) {
            $c->respond($e);
        }
    };

app->start();