package MojoBase::Exception::BasicException;

use strict;
use warnings FATAL => 'all';

use Carp qw(longmess);

sub new {
    my ($class, $clazz, $message, $code) = @_;
    $code ||= 500;

    return bless {
            class   => $clazz,
            message => $message,
            code    => $code,
            trace   => longmess(),
        }, $class;
}

sub code {
    return shift->{code};
}

sub message {
    return shift->{message};
}

sub trace {
    return shift->{trace};
}

sub class {
    return shift->{class};
}

1;