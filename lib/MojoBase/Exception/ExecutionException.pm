package MojoBase::Exception::ExecutionException;

use strict;
use warnings FATAL => 'all';

use MojoBase::Exception::BasicException;
use base qw(MojoBase::Exception::BasicException);

sub new {
    my ($class, $message, $code) = @_;
    return $class->SUPER::new($class, $message, $code);
}

1;