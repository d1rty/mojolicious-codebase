package MojoBase::Exception::UnauthorizedAccessException;

use strict;
use warnings FATAL => 'all';

use MojoBase::Exception::BasicException;
use base qw(MojoBase::Exception::BasicException);

sub new {
    my ($class) = @_;
    return $class->SUPER::new($class, "You are not authorized to perform this action on that resource", 403);
}

1;