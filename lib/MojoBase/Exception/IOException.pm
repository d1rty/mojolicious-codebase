package MojoBase::Exception::IOException;

use strict;
use warnings FATAL => 'all';

use MojoBase::Exception::BasicException;
use base qw(MojoBase::Exception::BasicException);

sub new {
    my ($class, $message) = @_;
    return $class->SUPER::new($class, $message, 500);
}

1;