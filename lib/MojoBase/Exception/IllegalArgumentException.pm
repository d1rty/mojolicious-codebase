package MojoBase::Exception::IllegalArgumentException;

use strict;
use warnings FATAL => 'all';

use MojoBase::Exception::BasicException;
use base qw(MojoBase::Exception::BasicException);

=cut
400 is the best choice in both cases. If you want to further clarify the error you can either change the Reason Phrase or include a body to explain the error.

412 - Precondition failed is used for conditional requests when using last-modified date and ETags.

403 - Forbidden is used when the server wishes to prevent access to a resource.

The only other choice that is possible is 422 - Unprocessable entity
=cut

sub new {
    my ($class, $message) = @_;
    return $class->SUPER::new($class, $message, 422);
}

1;
