package MojoBase::Exception::AbstractionException;

use strict;
use warnings FATAL => 'all';

use MojoBase::Exception::BasicException;
use base qw(MojoBase::Exception::BasicException);

sub new {
    my ($class, $method) = @_;
    return $class->SUPER::new(
        $class,
        sprintf('Method %s was declared abstract and must be redeclared by a subclass', $method), 422)
    ;
}

1;
