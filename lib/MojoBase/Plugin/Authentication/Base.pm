package MojoBase::Plugin::Authentication::Base;

use strict;
use warnings FATAL => 'all';

#@returns MojoBase::Plugin::Authentication::Base;
sub new {
    my ($class, $c) = @_;

    return bless {
            c       => $c,
            config  => $c->config->{$class},
        }, $class;
}

sub c {
    return shift->{c};
}

sub app {
    return shift->c->app;
}

sub config {
    return shift->{config};
}

sub authenticate {
    ## no critic (ErrorHandling::RequireCarping)
    die MojoBase::Exception::AbstractionException->new('authenticate');
}

1;