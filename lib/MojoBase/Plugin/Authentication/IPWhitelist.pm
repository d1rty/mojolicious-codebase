package MojoBase::Plugin::Authentication::IPWhitelist;

use strict;
use warnings FATAL => 'all';

use MojoBase::Plugin::Authentication::Base;
use base qw(MojoBase::Plugin::Authentication::Base);

sub authenticate {
    my ($self) = @_;

    my $whitelist_hosts = (defined $self->config && ref ($self->config) eq 'ARRAY')
        ? $self->config
        : [ ];

    return (grep { $self->c->remotehost->ip() eq $_ } @$whitelist_hosts);
}

1;