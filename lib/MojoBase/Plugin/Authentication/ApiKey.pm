package MojoBase::Plugin::Authentication::ApiKey;

use strict;
use warnings FATAL => 'all';

use MojoBase::Plugin::Authentication::Base;
use base qw(MojoBase::Plugin::Authentication::Base);

sub authenticate {
    my ($self) = @_;

    my $keyname = $self->config->{keyname};
    return 0 unless defined $keyname;

    my $key_content = $self->c->tx->req->headers->header( $self->config->{keyname} );
    return 0 if (!defined $key_content || $key_content eq '');

    return (grep { $key_content eq $_ } @{$self->config->{keys}});
}

1;