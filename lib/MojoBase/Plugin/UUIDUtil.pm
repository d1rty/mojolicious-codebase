package MojoBase::Plugin::UUIDUtil;

use strict;
use warnings FATAL => 'all';

use base 'Mojolicious::Plugin';

use MojoBase::Exception::IOException;

sub register {
    my ($self, $app, $config) = @_;

    $app->helper( 'uuid.regexp.lazy' => sub {
            ## no critic (RegularExpressions::ProhibitComplexRegexes)
            return qr/
                \{?
                    (?:uuid:)?
                    ([[:xdigit:]]{8})-?
                    ([[:xdigit:]]{4})-?
                    ([[:xdigit:]]{4})-?
                    ([[:xdigit:]]{4})-?
                    ([[:xdigit:]]{12})
                    \}?
                /x;
        } );

    $app->helper( 'uuid.regexp.hard' => sub {
            ## no critic (RegularExpressions::ProhibitComplexRegexes)
            return qr/
                ^
                    ([[:xdigit:]]{8})-?
                    ([[:xdigit:]]{4})-?
                    ([[:xdigit:]]{4})-?
                    ([[:xdigit:]]{4})-?
                    ([[:xdigit:]]{12})
                    $
                /x;
        } );

    $app->helper( 'uuid.toUUID' => sub {
            my ($c, $input, $regexp) = @_;
            $c->assert->defined( 'uuid', $input );
            $regexp ||= $c->uuid->regexp->hard();

            ## no critic (ErrorHandling::RequireCarping RegularExpressions::ProhibitCaptureWithoutTest)
            ($input =~ $regexp)
                ? return sprintf( "%s-%s-%s-%s-%s", $1, $2, $3, $4, $5 )
                : die MojoBase::Exception::IOException->new( sprintf( '%s could not be parsed to a UUID', $input ) );
        } );

    $app->helper( 'uuid.isUUID' => sub {
            my ($c, $input, $regexp) = @_;
            $c->assert->defined( 'uuid', $input );
            $regexp ||= $c->uuid->regexp->hard();

            return ($input =~ $regexp);
        } );

    return;
}

1;