package MojoBase::Plugin::ApiKeyGenerator;

use strict;
use warnings FATAL => 'all';

use base 'Mojolicious::Plugin';

use MojoBase::Exception::IOException;

use Data::UUID;

sub register {
    my ($self, $app, $config) = @_;

    $app->helper( 'apikey.exists' => sub {
            my ($c, $apikey) = @_;
            return (grep { $apikey eq $_ } @{ $app->config->{'MojoBase::Plugin::Authentication::ApiKey'}->{keys} });
        } );

    $app->helper( 'apikey.generate' => sub {
            my ($c) = @_;
            return Data::UUID->new()->create();
        } );

    $app->helper( 'apikey.add' => sub {
            my ($c) = @_;

            my $key;
            while (!defined $key) {
                $key = $c->apikey->generate();

                if ($c->apikey->exists( $key )) {
                    $key = undef;
                } else {
                    push @{ $app->config->{'MojoBase::Plugin::Authentication::ApiKey'}->{keys} },
                        $key;
                }
            }

            return $key;

        } );

    return;
}

1;