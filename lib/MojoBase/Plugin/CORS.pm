package MojoBase::Plugin::CORS;

use strict;
use warnings FATAL => 'all';

use base 'Mojolicious::Plugin';

sub register {
    my ($self, $app, $config) = @_;

    $app->helper('cors' => sub {
            my ($c) = @_;
            my $origin = $c->tx->req->headers->header( 'Origin' );

            # allow CrossSiteResourceSharing for allowed origins
            if (defined $origin && grep { $origin eq $_ } @{$config->{hosts}}) {
                $c->res->headers->header( 'Access-Control-Allow-Origin' => $origin );
            }

            $c->res->headers->append( 'Vary' => 'Origin' ); # otherwise CORS will be unreliable
            $c->res->headers->append( 'Vary' => 'Accept' ); # needed by content-negotiation
        });

    return;
}

1;