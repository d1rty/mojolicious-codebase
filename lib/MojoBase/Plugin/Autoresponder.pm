package MojoBase::Plugin::Autoresponder;

use strict;
use warnings FATAL => 'all';

use Mojo::Base 'Mojolicious::Plugin';

sub register {
    my ($self, $app, $config) = @_;

    $app->helper( 'respond' => sub {
            my ($c, $object, $status) = @_;
            $status ||= 200;

            # set exception status code and log exception
            if (ref ( $object ) =~ m{^.+Exception$}x) {
                $status = $object->code();

                $c->app->log->error( sprintf( 'Exception %s: %s',
                        $object->class(),
                        $object->message() )
                );
                $c->app->log->error( $object->trace() );
                # remove trace from output
                delete $object->{trace};
            }

            my $format = $c->serializer->formats();

            # set Content-Type header explicitly
            $c->res->headers->header( 'Content-Type' => $c->serializer->content_type( $format ) );
            $c->respond_to(
                $format => {
                    text   => $c->serializer->serialize( $object, $format ),
                    status => $status,
                }
            );
        } );

    return;
}

1;