package MojoBase::Plugin::Authentication;

use strict;
use warnings FATAL => 'all';

use base 'Mojolicious::Plugin';

use MojoBase::Exception::UnauthorizedAccessException;
use MojoBase::Plugin::Authentication::IPWhitelist;
use MojoBase::Plugin::Authentication::ApiKey;

use TryCatch;

sub register {
    my ($self, $app, $config) = @_;

    $app->helper('authenticate' => sub {
            my ($c) = @_;

            my $authenticators = $config->{plugins};
            return if (!defined $authenticators || ref ($authenticators) ne 'ARRAY' || !scalar @$authenticators);

            my $authentication_success = 0;
            foreach (@$authenticators) {
                my $authenticator = $_->new($c);

                if ($authenticator->authenticate()) {
                    $authentication_success = 1;
                    last;
                }
            }

            ## no critic (ErrorHandling::RequireCarping)
            die MojoBase::Exception::UnauthorizedAccessException->new() unless $authentication_success;
        });

    return;
}

1;