package MojoBase::Plugin::Assertions;

use strict;
use warnings FATAL => 'all';

use base 'Mojolicious::Plugin';

use MojoBase::Exception::IllegalArgumentException;
use Type::Tiny;

my $types = {
    defined     => Type::Tiny->new(
        name       => 'Defined',
        constraint => sub { defined $_ },
        message    => sub { 'undef value' },
    ),
    numeric     => Type::Tiny->new(
        name       => 'Number',
        constraint => sub { $_ =~ m{^\d+$}x },
        message    => sub { 'must be numeric' }
    ),
    not_numeric => Type::Tiny->new(
        name       => 'Not_A_Number',
        constraint => sub { $_ !~ m{^\d+$}x },
        message    => sub { 'must not be numeric' }
    ),
    hash        => Type::Tiny->new(
        name       => 'Hash',
        constraint => sub { ref ( $_ ) eq 'HASH' },
        message    => sub { 'value must be a hashref' }
    ),
    array       => Type::Tiny->new(
        name       => 'Array',
        constraint => sub { ref ( $_ ) eq 'ARRAY' },
        message    => sub { 'value must be an arrayref' }
    ),
};

$types->{string} = $types->{not_numeric};

sub register {
    my ($self, $app, $config) = @_;

    foreach my $method (keys %$types) {
        $app->helper( sprintf( 'assert.%s', $method ) => sub {
                my ($c, $desc, $obj) = @_;

                eval {
                    $types->{$method}->assert_valid( $obj );
                    return 1;
                } or do {
                    ## no critic (ErrorHandling::RequireCarping)
                    die MojoBase::Exception::IllegalArgumentException->new( sprintf(
                                'Parameter %s assertion failed: %s',
                                $desc,
                                    ($@->can('message')) ? $@->message() : '(no message)'
                            ) );

                };
            } );
    }

    $app->helper( 'assert.object' => sub {
            my ($c, $desc, $ref, $obj) = @_;

            ## no critic (ErrorHandling::RequireCarping)
            die MojoBase::Exception::IllegalArgumentException->new( sprintf(
                        'Parameter %s assertion failed: is not a %s object', $desc, $ref
                    ) ) unless ref ($obj) eq $ref;
        } );

    return;
}

1;