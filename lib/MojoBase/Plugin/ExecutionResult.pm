package MojoBase::Plugin::ExecutionResult;

use strict;
use warnings FATAL => 'all';

sub new {
    my ($class, $args) = @_;

    my $self = (ref ($args) eq 'HASH')
        ? bless $args, $class
        : bless {}, $class;

    return $self;
}

## no critic (ClassHierarchies::ProhibitAutoloading)
sub AUTOLOAD {
    my ($self, $value) = @_;
    our $AUTOLOAD;

    my ($method) = ($AUTOLOAD =~ m{([^:]+$)}x);
    return if $method eq 'DESTROY';

    $self->{$method} = $value if defined $value;
    return $self->{$method} || undef;
}

1;