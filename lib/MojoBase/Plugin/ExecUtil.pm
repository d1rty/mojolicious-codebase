package MojoBase::Plugin::ExecUtil;

use strict;
use warnings FATAL => 'all';

use base 'Mojolicious::Plugin';

use TryCatch;
use IO::CaptureOutput qw(capture_exec);
use MojoBase::Plugin::ExecutionResult;

=head1 NAME

MojoBase::Plugin::ExecUtil

=head1 SYNOPSIS

  use MojoBase::Plugin::ExecUtil;
  my $util = MojoBase::Plugin::ExecUtil->new( Mojolicious::Controller );

=head1 DESCRIPTION

This package is used for making superious system calls.

All systemcall commands return STDOUT, STDERR and the exit code inside of a hashref.

=head1 DEPENDENCIES

    IO::CaptureOutput

=cut


sub register {
    my ($self, $app, $config) = @_;

    $app->helper( 'executil.execute' => sub {
            my ($c, @args) = @_;

            my ($stdout, $stderr, $success, $exitcode) = capture_exec(@args);

            unless ($success) {
                $c->app->log->error( sprintf('Error while running command "%s": %s', join "\ ", @args, $stderr) );

                ## no critic (ErrorHandling::RequireCarping)
                die MojoBase::Exception::ExecutionException->new( $stderr,
                        500 + $exitcode ) if ($config->{die_on_error});
            }

            return MojoBase::Plugin::ExecutionResult->new( {
                    stdout   => $stdout,
                    stderr   => $stderr,
                    success  => $success,
                    exitcode => $exitcode,
                } );
        } );

    $app->helper( 'executil.sudo' => sub {
            my ($c, @args) = @_;

            if (scalar @args == 1) {
                return $c->executil->execute( sprintf('sudo %s', $args[0] ) );

            } else {
                unshift( @args, qw(sudo) );
                return $c->executil->execute( @args );

            }
        } );

    $app->helper( 'executil.fstype' => sub {
            my ($c, $item) = @_;
            try {
                my $result = $c->executil->sudo( "find", "-maxdepth", 0, $item,
                    '(', '-type', 'b', '-and', '-print', 'block_special', ')',
                    '(', '-type', 'c', '-and', '-print', 'character_special', ')',
                    '(', '-type', 'd', '-and', '-print', 'directory', ')',
                    '(', '-type', 'p', '-and', '-print', 'pipe', ')',
                    '(', '-type', 'f', '-and', '-print', 'file', ')',
                    '(', '-type', 'l', '-and', '-print', 'link', ')',
                    '(', '-type', 's', '-and', '-print', 'socket', ')',
                    '(', '-type', 'D', '-and', '-print', 'door', ')',
                );

                return $result || 'unknown_type';
            } catch (MojoBase::Exception::ExecutionException $e) {
                return 'absent';
            }

        } );

    return;
}


=head1 METHODS

=over 4

=item C<sudo>

  my $result = $util->execute("command", "arg0", "argN");

Executes some command. Returns a hashref with STDOUT, STDERR and the exitcode

=back

=cut

=over 4

=item C<sudo>

  my $result = $util->sudo("command", "arg0", "argN");

Executes some command with 'sudo sudocheck'. Returns a hashref with STDOUT, STDERR and the exitcode

=back

=cut


=over 4

=item C<file_type>

  my $file = $util->file_type("/directory/file");

Look up the filesystem object given as first paramater and return the type.

=back

=cut


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;

1;