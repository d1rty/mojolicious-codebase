package MojoBase::Plugin::RemoteHost;

use strict;
use warnings FATAL => 'all';

use base 'Mojolicious::Plugin';

sub register {
    my ($self, $app, $config) = @_;

    my $order = (defined $config->{order} && ref ($config->{order}) eq 'ARRAY')
        ? $config->{order}
        : ['x-real-ip', 'x-forwarded-for', 'tx'];

    $app->helper( 'remotehost.ip' => sub {
            my ($c) = @_;

            foreach my $place ( @{$order} ) {
                my $ip;

                if ( $place eq 'x-real-ip' ) {
                    $ip = $c->req->headers->header('X-Real-IP');

                } elsif ( $place eq 'x-forwarded-for' ) {
                    $ip = $c->req->headers->header('X-Forwarded-For');

                } elsif ( $place eq 'tx' ) {
                    $ip = $c->tx->remote_address;

                }

                return $ip if $ip;
            }

            return;
        });

    return;

}

1;