package MojoBase::Plugin::Serializer;

use strict;
use warnings FATAL => 'all';

use base 'Mojolicious::Plugin';

use Data::Serializer;
use HTML::Entities;

my $_default_format = 'json';

sub register {
    my ($self, $app, $config) = @_;

    # add custom content types
    foreach my $type (keys %{$config->{types}}) {
        $app->types->type( $type => $config->{types}->{$type} );
    }

    # set up serializers and deserializers
    my $formats = $config->{formats};

    # add default format to any response
    $formats->{any} = $formats->{default_format} || $formats->{$_default_format};

    # access Mojolicious accept formats from anywhere. Supports a default value
    $app->helper( 'serializer.formats' => sub {
            my ($c, $default) = @_;
            $default ||= 'any';

            my @formats = keys %{ $config->{formats} };
            unshift @formats, '';

            my $index = 0;
            $index++ until $formats[$index] eq 'any';
            splice(@formats, $index, 1);

            return $c->accepts( @formats ) || $default;
        } );

    $app->helper( 'serializer.settings' => sub {
            my ($c, $format) = @_;
            $format ||= 'any';
            return $formats->{$format};
        } );

    $app->helper( 'serializer.serializer' => sub {
            my ($c, $format) = @_;
            $format ||= 'any';
            return $c->serializer->settings( $format )->{serializer}->();
        } );

    $app->helper( 'serializer.content_type' => sub {
            my ($c, $format) = @_;
            $format ||= 'any';
            return $c->serializer->settings( $format )->{content_type};
        } );

    $app->helper( 'serializer.serialize' => sub {
            my ($c, $data, $format) = @_;

            # initialize serializer and serialize serializer
            return $c->serializer->serializer( $format )->serialize( $data );
        } );

    $app->helper( 'serializer.deserialize' => sub {
            my ($c, $data, $format) = @_;

            # default data is the request body
            $data ||= $c->req->body;

            if (!defined $format) {
                my $content_type = $c->tx->req->headers->header( 'Content-Type' );
                my $mojo_formats = $c->app->types->detect( $content_type );

                # apply default format if none could be detected
                push @{$mojo_formats}, $_default_format unless scalar @{$mojo_formats};

                $format = $mojo_formats->[0];
            }

            # initialize serializer and deserialize data
            return $c->serializer->serializer( $format )->deserialize( $data );
        } );

    return;

}

1;
