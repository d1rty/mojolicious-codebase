package MojoBase::Plugin::RestClient;
use strict;
use warnings FATAL => 'all';

use Mojo::Base 'Mojolicious::Plugin';

use REST::Client;
use MojoBase::Exception::IllegalArgumentException;

sub _fix_args {
    my ($self, $rest_settings, @args) = @_;

    if (scalar @args < 1) {
        ## no critic (ErrorHandling::RequireCarping)
        die MojoBase::Exception::IllegalArgumentException->new( 'Missing URI parameter for request' );
    }

    # args size is 1, assume URI is given -> add request method to first position (default GET)
    if (scalar @args == 1) {
        unshift @args, 'GET';
    }

    # args size is 2, assume URI and request method are given -> add request body (default empty)
    if (scalar @args == 2) {
        push @args, undef;
    }

    # args size is 3, assume URI, request method and request body are given -> add request headers (default empty hash)
    if (scalar @args == 3) {
        push @args, $rest_settings->{headers} || { };
    }

    return @args;
}

sub register {
    my ($self, $app, $config) = @_;

    $app->helper( 'client.rest.init' => sub {
            my ($c, $settings) = @_;

            my $client = REST::Client->new();
            $client->setHost( $settings->{host} ) if (defined $settings->{host});
            $client->setTimeout( $settings->{timeout} ) if (defined $settings->{timeout});

            return $client;
        } );

    $app->helper( 'client.rest.request' => sub {
            my ($c, $rest_settings, @args) = @_;

            my $client = $c->client->rest->init( $rest_settings );

            # redefine args
            @args = $self->_fix_args( $rest_settings, @args );

            # auto-serialize request body
            if (defined $args[2]) {
                $args[2] = $c->serializer->serialize(
                    $args[2],
                    $rest_settings->{serialize_format} || 'json'
                );
            }

            my $response = $client->request( @args );
            return $c->serializer->deserialize(
                $response->responseContent(), $rest_settings->{deserialize_format} || 'json'
            );
        } );

    return;
}

1;